# Ivo Javier Silva Sobarzo - Software

#  ESTE PROGRAMA TIENE COMO FUNCIÓN RESOLVER UN SUDOKU MEDIANTE
#  EL INGRESO DE UNA MATRIZ CON LAS PISTAS DEL SUDOKU, ES IMPORTANTE
#  MENCIONAR QUE EL PROGRAMA MUESTRA EL PRIMER RESULTADO ASOCIADO
#  A LOS NUMERO PRE-DEFINIDOS PUDIENDO EXISTIR MÁS SOLUCIONES
#  PARA LAS MISMAS PISTAS.


tabla0 = [

	[5,3,0,0,7,0,0,0,0],  #Numero pre-definidos para resolver el sudoku
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,7,9]

]


#FUNCION PARA VERIFICAR QUE EL NUMERO ES CORRECTO

def numero_valido(tabla0,fila,columna,numero):

	#NOS ASEGURAMOS QUE EL NUMERO NO SE ENCUENTRE EN LA MISMA FILA
	for i in range(9):
		if tabla0[fila][i] == numero:
			return False


	#NOS ASEGURAMOS QUE EL NUMERO NO SE ENCUENTRE EN LA MISMA COLUMNA
	for i in range(9):
		if tabla0[i][columna] == numero:
			return False

	#NOS ASEGURAMOS QUE EL NUMERO NO SE ENCUENTRE EN LA REGIÓN CORRESPONDIENTE
	for i in range(3):
		for j in range(3):
			if tabla0[(fila - fila % 3) + i][(columna - columna % 3) + j] == numero:
				return False

	return True

#MOSTRAR SUDOKU

def mostrar(tabla0):
	for k in tabla0:
		print(k)


#FUNCION PARA RESOLVER EL SUDOKU

def resolver(tabla0):
	for i in range(9):
		for j in range(9):
			if tabla0[i][j] == 0:   #BUSCAMOS SOLO LAS CASILLAS VACÍAS PARA CAMBIAR SU VALOR
				for n in range(1,10):  
					if numero_valido(tabla0,i,j,n):
						tabla0[i][j] = n #SI EL NUMERO ES VALIDO LO INSERTAMOS
						resolver(tabla0)
						tabla0[i][j] = 0  #SI EL NUMERO NO ES VALIDO NO SE ALTERA
				return
	mostrar(tabla0)  #RESULTADO DEL SUDOKU

resolver(tabla0)



